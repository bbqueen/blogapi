using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace BlogApi.Models

{
    public class BlogPost
    {
        [Key]
        public int Id { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        [ForeignKey("Blogger")]
        public int AuthorId { get; set; }
        public IList<LinkDto> Links { get; set; }

    }
}