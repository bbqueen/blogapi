using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace BlogApi.Models

{
    public class Blogger
    {
        [Key]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual IList<BlogPost> Posts { get; set; }
        public IList<LinkDto> Links { get; set; }

    }
}