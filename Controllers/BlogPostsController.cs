using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BlogApi.Models;

namespace BlogApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogPostsController : ControllerBase
    {
        private readonly BlogContext _context;

        public BlogPostsController(BlogContext context)
        {
            _context = context;
        }

        // GET: api/BlogPosts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BlogPost>>> GetBlogPosts()
        {
            return await _context.BlogPosts.ToListAsync();
        }

        // GET: api/BlogPosts/5
        [HttpGet("{id:int}", Name = nameof(GetBlogPost))]
        public async Task<ActionResult<BlogPost>> GetBlogPost(int id)
        {
            var blogPost = await _context.BlogPosts.FindAsync(id);

            if (blogPost == null)
            {
                return NotFound();
            }

            return CreateLinksForUser(blogPost);
        }

        // PUT: api/BlogPosts/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id:int}", Name = nameof(PutBlogPost))]
        public async Task<IActionResult> PutBlogPost(int id, BlogPost blogPost)
        {
            if (id != blogPost.Id)
            {
                return BadRequest();
            }

            _context.Entry(blogPost).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BlogPostExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/BlogPosts
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost("", Name = nameof(PostBlogPost))]
        public async Task<ActionResult<BlogPost>> PostBlogPost(BlogPost blogPost)
        {
            _context.BlogPosts.Add(blogPost);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (BlogPostExists(blogPost.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            //return CreatedAtAction("GetBlogPost", new { id = blogPost.Id }, blogPost);
            return CreatedAtAction(nameof(GetBlogPost), new { id = blogPost.Id }, CreateLinksForUser(blogPost));
        }

        // DELETE: api/BlogPosts/5
        [HttpDelete("{id:int}", Name = nameof(DeleteBlogPost))]
        public async Task<ActionResult<BlogPost>> DeleteBlogPost(int id)
        {
            var blogPost = await _context.BlogPosts.FindAsync(id);
            if (blogPost == null)
            {
                return NotFound();
            }

            _context.BlogPosts.Remove(blogPost);
            await _context.SaveChangesAsync();

            return CreateLinksForUser(blogPost);
        }

        [HttpGet("{id:int}/Blogger", Name = nameof(GetBlogPostAuthor))]
        public async Task<ActionResult<Blogger>> GetBlogPostAuthor(int id)
        {
            var blogPost = await _context.BlogPosts.FindAsync(id);
            var blogger = await _context.Bloggers.FindAsync(blogPost.AuthorId);
            if (blogPost == null)
            {
                return NotFound("BlogPost not found");
            }
            if (blogger == null)
            {
                return NotFound("Blogger not found");
            }
            return RedirectToAction("GetBlogger", "Bloggers", new { id = blogger.Id });
        }

        public bool BlogPostExists(int id)
        {
            return _context.BlogPosts.Any(e => e.Id == id);
        }
        public BlogPost CreateLinksForUser(BlogPost blogPost)
        {
            var idObj = new { id = blogPost.Id };
            List<LinkDto> links = new List<LinkDto>();

            links.Add(
                new LinkDto(Url.Link(nameof(this.GetBlogPost), idObj),
                "self",
                "GET"));

            links.Add(
                new LinkDto(Url.Link(nameof(this.PutBlogPost), idObj),
                "update_blogPost",
                "PUT"));


            links.Add(
                new LinkDto(Url.Link(nameof(this.DeleteBlogPost), idObj),
                "delete_blogPost",
                "DELETE"));

            links.Add(
                new LinkDto(Url.Link(nameof(this.PostBlogPost), idObj),
                "add_blogPost",
                "POST"));

            links.Add(
                new LinkDto(Url.Link(nameof(this.GetBlogPostAuthor), idObj),
                "get_blogPost_author",
                "GET"));

            blogPost.Links = links;
            return blogPost;
        }
    }
}
