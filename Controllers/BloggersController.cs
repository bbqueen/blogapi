using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BlogApi.Models;

namespace BlogApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BloggersController : ControllerBase
    {
        private readonly BlogContext _context;

        public BloggersController(BlogContext context)
        {
            _context = context;
        }

        // GET: api/Bloggers
        [HttpGet("", Name = nameof(GetBloggers))]
        public async Task<ActionResult<IEnumerable<Blogger>>> GetBloggers()
        {
            return await _context.Bloggers.ToListAsync();
        }

        // GET: api/Bloggers/5
        [HttpGet("{id:int}", Name = nameof(GetBlogger))]
        public async Task<ActionResult<Blogger>> GetBlogger(int id)
        {
            var blogger = await _context.Bloggers.FindAsync(id);
            blogger.Posts = getBloggerPostsFromDb(id).ToList();
            if (blogger == null)
            {
                return NotFound();
            }

            return this.CreateLinksForUser(blogger);
        }

        // PUT: api/Bloggers/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id:int}", Name = nameof(PutBlogger))]
        public async Task<IActionResult> PutBlogger(int id, Blogger blogger)
        {
            if (id != blogger.Id)
            {
                return BadRequest();
            }

            _context.Entry(blogger).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BloggerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Bloggers
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost("", Name = nameof(PostBlogger))]
        public async Task<ActionResult<Blogger>> PostBlogger(Blogger blogger)
        {
            _context.Bloggers.Add(blogger);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (BloggerExists(blogger.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            //return CreatedAtAction("GetBlogger", new { id = blogger.Id }, blogger);
            return CreatedAtAction(nameof(GetBlogger), new { id = blogger.Id }, CreateLinksForUser(blogger));
        }

        // DELETE: api/Bloggers/5
        [HttpDelete("{id:int}", Name = nameof(DeleteBlogger))]
        public async Task<ActionResult<Blogger>> DeleteBlogger(string id)
        {
            var blogger = await _context.Bloggers.FindAsync(id);
            if (blogger == null)
            {
                return NotFound();
            }

            _context.Bloggers.Remove(blogger);
            await _context.SaveChangesAsync();

            return this.CreateLinksForUser(blogger);
        }

        // GET: api/Bloggers/5/BlogPosts
        [HttpGet("{id:int}/BlogPosts", Name = nameof(GetBloggerPosts))]
        public async Task<ActionResult<IEnumerable<BlogPost>>> GetBloggerPosts(int id)
        {
            var blogger = await _context.Bloggers.FindAsync(id);
            if (blogger == null)
                {
                    return NotFound("Blogger doesn't exist");
                }
            var blogPosts =  getBloggerPostsFromDb(id).ToList(); // contournement car persistence des blogposts non trouvée
        
            if (blogger == null)
            {
                return NotFound("Blogger doesn't exist");
            }
            return CreatedAtAction(nameof(GetBloggerPosts), new { id = blogger.Id }, blogPosts);
        }

        // POST: api/Bloggers/5/BlogPosts
        [HttpPost("{id:int}/BlogPosts", Name = nameof(AddBloggerPost))]
        public async Task<ActionResult<BlogPost>> AddBloggerPost(int id, BlogPost blogPost)
        {
            var blogger = await _context.Bloggers.FindAsync(id);
            if (blogger == null)
                {
                    return NotFound("Blogger doesn't exist");
                }
            blogger.Posts = getBloggerPostsFromDb(id).ToList();

            
            bool userHasPostWithSameId = blogger.Posts.Any(post => post.Id.Equals(blogPost.Id));

            if (userHasPostWithSameId)
            {
                return Conflict("BlogPost id exists");
            }

            blogPost.AuthorId = id;
            _context.BlogPosts.Add(blogPost);
            blogger.Posts.Add(blogPost);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                throw;
            }

            //return RedirectToAction("PostBlogPost", "BlogPosts", new BlogPost{AuthorId=id, Id = blogPost.Id, Body=blogPost.Body, Subject=blogPost.Subject});
            return CreatedAtAction(nameof(GetBlogger), new { id = blogger.Id }, CreateLinksForUser(blogger));

        }

        // DELETE: api/Bloggers/5/BlogPosts
        [HttpDelete("{id:int}/BlogPosts", Name = nameof(DeleteBloggerPosts))]
        public async Task<ActionResult<IEnumerable<BlogPost>>> DeleteBloggerPosts(int id)
        {
            var blogger = await _context.Bloggers.FindAsync(id);
            if (blogger == null)
            {
                return NotFound("Blogger doesn't exist");
            }
            var bloggerPosts = getBloggerPostsFromDb(id);

            foreach (var post in bloggerPosts)
            {
                _context.BlogPosts.Remove(post);
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (!BloggerExists(blogger.Id))
                {
                    return NotFound("Blogger doesn't exist");
                }

                else
                {
                    throw;
                }
            }

            //return CreatedAtAction("GetBlogger", new { id = blogger.Id }, blogger);
            return CreatedAtAction(nameof(GetBlogger), new { id = blogger.Id }, CreateLinksForUser(blogger));

        }

        [HttpPut("{connecterBloggerId:int}/ConnectTo/{targetBloggerId:int}", Name = nameof(ConnectToBlogger))]
        public async Task<ActionResult<Blogger>> ConnectToBlogger(int connecterBloggerId, int targetBloggerId)
        {
            var connecterBlogger = await _context.Bloggers.FindAsync(connecterBloggerId);
            if (connecterBlogger == null)
            {
                return NotFound("Blogger doesn't exist");
            }
            connecterBlogger.Posts = getBloggerPostsFromDb(connecterBloggerId).ToList();

            var targetBlogger = await _context.Bloggers.FindAsync(connecterBloggerId);
            if (targetBlogger == null)
            {
                return NotFound("Target Blogger doesn't exist");
            }

            List<Connection> bloggerConnections = getConnectionsFromDb(connecterBloggerId).ToList();
            bool bloggersAlreadyConnected = bloggerConnections.Any(conn => conn.TargetBloggerId == targetBloggerId);

            if (bloggersAlreadyConnected)
            {
                return Conflict("Blogger already connected to target");
            }

            Connection newConnection = new Connection();
            newConnection.TargetBloggerId = targetBloggerId;
            newConnection.ConnecterBloggerId = connecterBloggerId;

            _context.Connections.Add(newConnection);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                    throw;
            }

        
            return CreatedAtAction(nameof(ConnectToBlogger), new { id = connecterBloggerId }, CreateLinksForUser(connecterBlogger));
        }

        [HttpGet("{connecterBloggerId:int}/DistanceTo/{targetBloggerId:int}", Name = nameof(DistanceToBlogger))]
        public async Task<ActionResult<Blogger>> DistanceToBlogger(int connecterBloggerId, int targetBloggerId)
        {
            var connecterBlogger = await _context.Bloggers.FindAsync(connecterBloggerId);
            if (connecterBlogger == null)
            {
                return NotFound("Blogger doesn't exist");
            }
            connecterBlogger.Posts = getBloggerPostsFromDb(connecterBloggerId).ToList();

            var targetBlogger = await _context.Bloggers.FindAsync(connecterBloggerId);
            if (targetBlogger == null)
            {
                return NotFound("Target Blogger doesn't exist");
            }

            List<Connection> bloggerConnections = getConnectionsFromDb(connecterBloggerId).ToList();
            bool bloggersAlreadyConnected = bloggerConnections.Any(conn => conn.TargetBloggerId == targetBloggerId);

            if (bloggersAlreadyConnected)
            {
                return Conflict("Blogger already connected to target");
            }
            int initialDistance = 0;
            int distanceToTarget = searchTargetBlogger(targetBloggerId, initialDistance , bloggerConnections);
            
            if(distanceToTarget == 0){
                return NotFound("Target Blogger cannot be connected following current connections");
            }
        
            return CreatedAtAction(nameof(DistanceToBlogger), new { id = connecterBloggerId }, distanceToTarget);
        }

        private IEnumerable<BlogPost> getBloggerPostsFromDb(int bloggerId){
            return _context.BlogPosts.Where(post => post.AuthorId.Equals(bloggerId));
        }
        private IEnumerable<Connection> getConnectionsFromDb(int bloggerId){
            return _context.Connections.Where(conn => conn.ConnecterBloggerId.Equals(bloggerId));
        }

        private int searchTargetBlogger(int targetBloggerId, int distance, List<Connection> connections){
            distance++;
            Connection foundTarget = connections.Find( conn => conn.TargetBloggerId.Equals(targetBloggerId));
            if(foundTarget != null){
                return distance;
            }else{
                List<Connection> connectionsOfConnections = connections.Select(conn => conn.TargetBloggerId)
                                                                .SelectMany(bloggerId => getConnectionsFromDb(bloggerId)) // for each targetblogger we get his connections (one level deeper)
                                                                .GroupBy(conn => conn.TargetBloggerId) // keep unique targets from the sublevel
                                                                .Select(group => group.First())
                                                                .ToList();

                if(!connectionsOfConnections.Any()){
                    return 0;
                }
                return searchTargetBlogger(targetBloggerId, distance, connectionsOfConnections);
                
                
            }
        }

        private bool BloggerExists(int id)
        {
            return _context.Bloggers.Any(e => e.Id == id);
        }

        public Blogger CreateLinksForUser(Blogger blogger)
        {
            var idObj = new { id = blogger.Id };
            List<LinkDto> links = new List<LinkDto>();

            links.Add(
                new LinkDto(Url.Link(nameof(this.GetBlogger), idObj),
                "self",
                "GET"));

            links.Add(
                new LinkDto(Url.Link(nameof(this.PutBlogger), idObj),
                "update_blogger",
                "PUT"));


            links.Add(
                new LinkDto(Url.Link(nameof(this.DeleteBlogger), idObj),
                "delete_blogger",
                "DELETE"));

            links.Add(
                new LinkDto(Url.Link(nameof(this.PostBlogger), idObj),
                "add_blogger",
                "POST"));

            links.Add(
                new LinkDto(Url.Link(nameof(this.GetBloggerPosts), idObj),
                "get_blogger_posts",
                "GET"));

            links.Add(
                new LinkDto(Url.Link(nameof(this.AddBloggerPost), idObj),
                "add_post_to_blogger_posts",
                "POST"));

            links.Add(
                new LinkDto(Url.Link(nameof(this.DeleteBloggerPosts), idObj),
                "delete_all_posts_of_blogger",
                "DELETE"));
            
            links.Add(
                new LinkDto(Url.Link(nameof(this.ConnectToBlogger), idObj),
                "connect_blogger_to_another",
                "PUT"));

            
            blogger.Links = links;
            return blogger;
        }
    }
}
