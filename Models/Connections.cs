using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace BlogApi.Models

{
    public class Connection{

    
        [Key]
        public int Id { get; set; }

        [ForeignKey("Blogger")]
        public int ConnecterBloggerId { get; set; }
        
        [ForeignKey("Blogger")]
        public int TargetBloggerId { get; set; }
    

    }
}